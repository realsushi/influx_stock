from datetime import datetime
from dotenv import load_dotenv, main
import os
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS, ASYNCHRONOUS
import csv

from database import token, org, bucket
from query import Stockqueries


class InfluxClient:
    def __init__(self, token, org, bucket):
        self._org = org
        self._bucket = bucket
        self._client = InfluxDBClient(url="http://localhost:8087", token=token)

    def query_data(self, query):
        query_api = self._client.query_api()
        result = query_api.query(org=self._org, query=query)
        results = []
        for table in result:
            for record in table.records:
                results.append((record.get_value(), record.get_field()))
        print(results)
        return results

    def write_data(self, data, write_option=SYNCHRONOUS):
        write_api = self._client.write_api(write_option)
        write_api.write(self._bucket, self._org, data, write_precision='s')

    def delete_data(self, measurement):
        delete_api = self._client.delete_api()
        start = "1970-01-01T00:00:00Z"
        stop = "2021-10-30T00:00:00Z"
        delete_api.delete(start, stop, f'_measurement="{measurement}"', bucket=self._bucket, org=self._org)



# DATA
url1 = "https://raw.githubusercontent.com/rahulbanerjee26/InfluxDB-Tutorial/main/Data/MSFT.csv"
url2 = "https://raw.githubusercontent.com/rahulbanerjee26/InfluxDB-Tutorial/main/Data/AAPL.csv"

# MAIN
IC = InfluxClient(token, org, bucket)
# IC.write_data(["MSFT,stock=MSFT Open=62.79,High=63.84,Low=62.13"])

# DATA
'''
Write Data for MSFT Stock
'''
MSFT_file = open('MSFT.csv')
csvreader = csv.reader(MSFT_file)
header = next(csvreader)
rows = []
for row in csvreader:
    date, open, high, low = row[0], row[1], row[2], row[3]
    line_protocol_string = ''
    line_protocol_string += f'MSFT_{date},'
    line_protocol_string += f'stock=MSFT '
    line_protocol_string += f'Open={open},High={high},Low={low} '
    line_protocol_string += str(int(datetime.strptime(date, '%Y-%m-%d').timestamp()))
    rows.append(line_protocol_string)

IC.write_data(rows)

IC.query_data(Stockqueries.query1())
IC.query_data(Stockqueries.query2())



