
class Stockqueries():
    def query1(self):
        '''
            Return the High Value for MSFT stock for since 1st October,2021
        '''
        query1 = 'from(bucket: "bucket1")\
        |> range(start: 1633124983)\
        |> filter(fn: (r) => r._field == "High")\
        |> filter(fn: (r) => r.stock == "MSFT")'
        return query1
        # IC.query_data(query1)
        # IC.delete_data("MSFT_2021-10-29")

    def query2(self):
        '''
            Return the High Value for the MSFT stock on 2021-10-29
        '''
        query2 = 'from(bucket: "bucket1")\
        |> range(start: 1633124983)\
        |> filter(fn: (r) => r._field == "High")\
        |> filter(fn: (r) => r._measurement == "MSFT_2021-10-29")'
        return query2
        # IC.query_data(query2)