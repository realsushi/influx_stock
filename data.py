#import pandas as pd
import csv
import datetime

url3 = "https://raw.githubusercontent.com/rahulbanerjee26/InfluxDB-Tutorial/main/Data/MSFT.csv"
#df = pd.read_csv(url)
#data = pd.("MSFT", start="2021-01-01", end="2021-10-30")


# DATA
'''
Write Data for MSFT Stock
'''
MSFT_file = open('MSFT.csv')
csvreader = csv.reader(MSFT_file)
header = next(csvreader)
rows = []
for row in csvreader:
    date, open, high, low = row[0], row[1], row[2], row[3]
    line_protocol_string = ''
    line_protocol_string += f'MSFT_{date},'
    line_protocol_string += f'stock=MSFT '
    line_protocol_string += f'Open={open},High={high},Low={low} '
    line_protocol_string += str(int(datetime.strptime(date, '%Y-%m-%d').timestamp()))
    rows.append(line_protocol_string)
