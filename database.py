from influxdb_client import InfluxDBClient
from dotenv import load_dotenv, main
import os

# DATABASE
load_dotenv()
token = os.getenv('TOKEN')
org = os.getenv('ORG')
bucket = os.getenv('BUCKET')

